const express = require('express');
const xml = require('xml');
const xmlparser = require('express-xml-bodyparser')
let Parser = require('rss-parser');
const cors = require('cors')

const app = express();
app.use(express.json())
app.use(xmlparser());
app.use(cors());

const port = 9001;
let data = []

app.get('/', (req, res) => {
    return res.send(data);

});

app.get('/hero/tambah', (req, res) => {
    let body = req.body;
    let query = req.query;
    data.push(query);
    res.send({
        sukses: true
    })
})

app.listen(port, () => {
    console.log(`Port ${port} sedang berjalan`);
})